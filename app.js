var express = require('express');
var exphbs  = require('express-handlebars');
var session = require('express-session');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var csrf = require('csurf');

var app = express();

app.set('views', __dirname + '/views');
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(express.static(path.join(__dirname + '/assets')));

var csrfProtection = csrf({ cookie: true });
var parseForm = bodyParser.urlencoded({ extended: false });

app.use(session({
  secret: 'pruebaSecret',
  resave: true,
  saveUninitialized: true
}));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

var loginRoute = require('./routes/login');
var inicioRoute = require('./routes/inicio');
var usuariosRoute = require('./routes/registroUsuarios');
app.use(csrf({ cookie: true }));
app.use('/inicio', inicioRoute);
app.use('/login', loginRoute);
app.use('/registroUsuarios', usuariosRoute);

var sess;
//página inicio
app.get('/', csrfProtection, function(req,res){
  sess = req.session;
  //verificar sesión
  if(sess.correo) {
      res.redirect('/inicio');
  }
  else {
      res.render('login', { csrfToken: req.csrfToken(), title : "Login" });
  }
});

module.exports = app;
app.listen(3000);
