-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.13 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para pruebaevolution
CREATE DATABASE IF NOT EXISTS `pruebaevolution` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `pruebaevolution`;

-- Volcando estructura para tabla pruebaevolution.tarea
CREATE TABLE IF NOT EXISTS `tarea` (
  `idtarea` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `prioridad` varchar(45) DEFAULT NULL,
  `vencimiento` datetime NOT NULL,
  `idusuario` int(11) NOT NULL,
  PRIMARY KEY (`idtarea`),
  KEY `usuario_tarea_FK_idx` (`idusuario`),
  CONSTRAINT `usuario_tarea_FK` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Volcando datos para la tabla pruebaevolution.tarea: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tarea` DISABLE KEYS */;
INSERT INTO `tarea` (`idtarea`, `nombre`, `prioridad`, `vencimiento`, `idusuario`) VALUES
	(1, 'Obtener requerimientos de software', '6', '2018-01-01 01:00:00', 1),
	(2, 'Diseñar software', '6', '2019-01-01 01:00:00', 1),
	(4, 'Taira', '4', '2018-11-04 01:00:00', 1),
	(5, 'prueba', '1', '2018-11-05 01:00:00', 1);
/*!40000 ALTER TABLE `tarea` ENABLE KEYS */;

-- Volcando estructura para tabla pruebaevolution.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `nombre2` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) NOT NULL,
  `apellido2` varchar(45) DEFAULT NULL,
  `correo` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `idusuario_UNIQUE` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Volcando datos para la tabla pruebaevolution.usuario: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`idusuario`, `nombre`, `nombre2`, `apellido`, `apellido2`, `correo`, `password`) VALUES
	(1, 'William', 'Andrés', 'Salazar', 'Gómez', 'srwilliamg@gmail.com', '123456'),
	(2, 'William', NULL, 'ramirez', '', 'pepe@prueba.com', '123');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
