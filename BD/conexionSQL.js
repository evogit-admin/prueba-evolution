var mysql = require('mysql');

var connection = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "root!",
  database : 'pruebaevolution',
  multipleStatements: true
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Conectado a base de datos");
});

module.exports = connection;
