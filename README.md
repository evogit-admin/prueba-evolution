**INSTRUCCIONES**

1.  En la carpeta ScriptSQL está la base de datos del sistema, ejecutar este script y crearla, si hay algún problema en la ejecución, crear la base de datos vacía con el nombre *pruebaevolution*
2.  Usar el comando *npm install* para agregar la carpeta *node_modules* copn las dependencias del proyecto.
3.  En la carpeta BD, modificar el archivo llamado *conexionSQL.js*, cambiar la línea **5** y **6**, por el nombre de usuario y contraseña de un perfil con permisos de usar la BD.
4.  En la consola de comando sea CMD o Git Bash (windows) correr el programa usando *node app.js*.
5.  Ingresar a la dirección **localhost:3000**
