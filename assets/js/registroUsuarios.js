$(document).ready(function() {
  validarCampos();
});

$(document).off('click', '#btn-registro').on('click', '#btn-registro', function() {
  var correo = $('#correoForm').val();
  var password = $('#passwordForm').val();
  var password2 = $('#password2Form').val();
  var nombre = $('#nombreForm').val();
  var nombre2 = $('#nombre2Form').val();
  var apellido = $('#apellidoForm').val();
  var apellido2 = $('#apellido2Form').val();

  if(password != password2){
    swal({
      title: 'Error',
      text: "Las contraseñas no son iguales.",
      icon: 'info',
      button: {
        text: 'Entendido',
        value: true
      }
    });
  }
  else{
    if(noNulo(correo) || noNulo(password) || noNulo(nombre) || noNulo(apellido)){
      swal({
        title: 'Información',
        text: "Llena los campos obligatorios.",
        icon: 'info',
        button: {
          text: 'Entendido',
          value: true
        }
      });
    }
    else{
      var data = {
        nombre: nombre,
        nombre2: nombre2,
        apellido: apellido,
        apellido2: apellido2,
        correo: correo,
        password: password
      };

      var stringData = utf8_to_b64(JSON.stringify(data));

      $.ajax({
        type: "POST",
        url: "registroUsuarios/crearUsuario",
        data: {
          data: stringData
        },
        headers: {
          'X-CSRF-TOKEN': csrfToken
        }
      }).done(function(data) {
        var respuesta = $.parseJSON(b64_to_utf8(data));if (respuesta.status) {

          swal({
            title: 'Registro',
            text: "Guardado correctamente.",
            icon: 'success',
            button: {
              text: 'Entendido',
              value: true
            }
          });
        }
        else {
          swal({
            title: 'Error',
            text: "Ha ocurrido un error.",
            icon: 'warning',
            button: {
              text: 'Entendido',
              value: true
            }
          });
        }

        window.location = "/";
      }).fail(function(data) {
        alert("La operación no ha podido ser completada, verifique su conexión a internet.");
      });
    }
  }
});

function validarCampos() {
  ValidarCorreo('#correoForm');
  ValidarTexto('#passwordForm');
  ValidarTexto('#password2Form');
  ValidarTexto('#nombreForm');
  ValidarTexto('#nombre2Form');
  ValidarTexto('#apellidoForm');
  ValidarTexto('#apellido2Form');
}

function noNulo(param) {
  return param == null || param == undefined || param == "";
}
